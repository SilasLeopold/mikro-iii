= Tools der Wahl für diesen Kurs

== Software Entwicklung

=== Code Erzeugung

:experimental:

Für kleinere Codebeispiele werden wir den https://godbolt.org[CompilerExplorer] verwenden. Durch den Einsatz des https://godbolt.org[CompilerExplorers] ist es uns möglich kurze Quelltextfragmente auszutauschen. Für größere Projekte verwenden wir die Entwicklungsumgebung <<visual-studio-code>> von icon:microsoft[] Microsoft(TM).

=== Git

==== GitLab

Jeder Student muss über einen GitLab Zugang verfügen. Dieser sollte im Idealfall über die DHBW E-Mail Adresse erfolgen. Siehe https://gitlab.com/users/sign_up .

[[git-bash]]
==== Git Bash

Die Vorlesung wird begleitet von einem git Projekt. Wir werden dieses gemeinsam mit Leben füllen. Am Ende der Vorlesung steht Ihnen ein umfangreiches Projekt zu Verfügung. Um das Ziel einer kooperativen Entwicklung zu erreichen wird eine Platform für die Kollaboration benötigt. Diese Platform stellt icon:gitlab[] https://about.gitlab.com/[GitLab] dar. Um die in der Vorlesung gezeigten `git` Befehle nachvollziehen zu können benötigen Sie einen `git` Kommandozeilenklienten. Die Installationsanweisung finden Sie auf der https://git-scm.com/downloads[git Webseite]. Eine Einführung in die Quellcodeverwaltung `git` ist im Kapitel <<git>> beschrieben.

NOTE: Bei Verwendung des Editors `vi / vim` ( in der Regel Standard bei git in der Konsole), ist es gut zu wissen das man den Edit-Mode mittels kbd:[Esc] verlässt. Möchte man den Editor beenden und das Dokument speichern so gibt man `:wq` im Kommando Mode ein und bestätigt mit der kbd:[Enter] Taste. Wollen Sie den Editor ohne zu speichern beenden so geben Sie nach dem Wechsel in den Kommando Modus `:q!` ein und bestätigen dies mit der kbd:[Enter] Taste. Um in den insert modus zu kommen kann man mittels kbd:[i] zum 'Insert Modus' wechseln.

TIP: Unter einer aktuellen Version von icon:ubuntu[] Ubuntu Linux kann `git` über die Paketquellen installiert werden: `apt-get update && apt-get install git git-gui` setzen Sie eine andere icon:linux[] Distribution ein so lohnt sich ein Blick in die Paketquellen. Bei Verwendung eines icon:apple[] Apple Betriebssystem lohnt sich unterumständen der Einsatz von https://brew.sh/[Homebrew].


[[visual-studio-code]]
=== Visual Studio Code

Da Visual Studio Code, im Folgenden nur noch VSCode genannt, auf den gängigsten Betriebssystemen zu Verfügung steht werden wir im Weiteren Verlauf diese IDE zum Entwickeln verwenden. Bei Visual Studio Code handelt es sich strenggenommen nicht um eine IDE sondern um einen sehr gut erweiterbaren Editor. Informationen zur Installation von Visual Studio Code finden Sie in der https://code.visualstudio.com/docs/setup/setup-overview[VSCode Online Hilfe]

==== Installation C/{cpp} Compiler

VSCode verfügt nicht über einen eignen C/{cpp} Compiler. Zum Entwickeln von {cpp} Quellcode benötigen wird ein entsprechender Compiler für Ihre Platform benötigt. Sie finden eine Übersicht zur Installation des Compilers für Ihre Platform im Bereich https://code.visualstudio.com/docs/languages/cpp[C/C++ Support] der Online Hilfe.

MSYS
Aus Erfahrung mit Windows hat sich die Installation vom https://www.msys2.org/wiki/MSYS2-installation/[msys] System bewährt.
Wir haben im Kurs 2020 die Version msys2-x86_64-20200903 verwendet und erfolgreich benutzt.
Falls sie msys verwenden, kann man mittels `pacman -S gcc` in der msys Konsole den gcc Kompiler installieren.
Zusätzlich sollte man auch das Programm `make` installieren mittels `pacman -S make` ebenfalls in der msys Konsole.

[[visual-studio-code-erweiterungen]]
==== Visual Studio Code Erweiterungen

Wie bereits erwähnt bietet Visual Studio Code die Möglichkeit durch https://code.visualstudio.com/docs/editor/extension-gallery[Erweiterungen] noch einfacher bedient zu werden. Die Installation lässt sehr einfach auf der https://code.visualstudio.com/docs/editor/extension-gallery#_command-line-extension-management[Kommandozeile] durchführen.

.Anzeige der Installierten Erweiterungen
....
code --list-extensions
....

.Installation einer Erweiterung
....
code --install-extension (<extension-id> | <extension-vsix-path>)
    Installs an extension.
....

Die folgenden Erweiterungen sind zu installieren:

* `ms-vscode.cmake-tools` https://marketplace.visualstudio.com/items?itemName=ms-vscode.cmake-tools[CMake Tools]
* `ms-vscode.cpptools` https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools[C/C++]
* `mhutchie.git-graph` https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph[Git Graph]
* `eamodio.gitlens`  https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens[GitLens]


[[cmake-installation]]
=== Installation von CMake

Für eine {cpp} Entwicklung in komplexeren Projekten ist eine Aufteilung in verschiedene Komponenten Notwendig. Damit diese möglichst effizient und Platform übergreifend übersetzt werden können gibt es eine Vielzahl an Buildsystemen. Da es nicht einfach ist diese Vielzahl an Systemen zu unterstützen hat sich https://cmake.org/[CMake] als Generator für die jeweiligen Buildsysteme etabliert. Für unser gemeinsames Projekt verwenden wir CMake. Bitte installieren https://cmake.org/download/[CMake für Ihr Betriebssystem].

TIP: Unter Windows empfehle ich die Verwendung des `.msi` Installers. Bitte achten Sie hier darauf, dass sie CMake zum System Pfad hinzufügen. Sollten Sie unter einer aktuellen icon:ubuntu[] Ubuntu Linux Version arbeiten reicht die Installation mittels `apt-get update && apt-get install cmake` setzen Sie eine andere icon:linux[] Distribution ein so lohnt sich auch dort ein Blick in die Paketquellen. Bei Verwendung eines icon:apple[] Apple Betriebssystem lohnt sich unterumständen der Einsatz von https://brew.sh/[Homebrew].

== Checkliste

Bitte prüfen Sie ob Sie diese Tools installiert haben.

.Installation Development Tools
* [ ] Installation der <<git-bash>>
* [ ] <<visual-studio-code>> Installation
* [ ] <<cmake-installation>>
* [ ] <<visual-studio-code-erweiterungen>> einrichten

Bitte prüfen Sie auch ob Sie einen GitLab Zugang besitzen.

== Nützliche Links

* https://stackoverflow.com ein sehr umfangreicher und guter Platz für Technische Fragen und Antworten.


<<<